resource "hcloud_server" "micro-instance" {
  name        = "bot-node"
  image       = "ubuntu-16.04"
  server_type = "cx11"
  ssh_keys    = ["${hcloud_ssh_key.default.name}"]
  location    = "hel1"
  backups     = false
}

output "public_ip4" {
  value = "${hcloud_server.micro-instance.ipv4_address}"
}
